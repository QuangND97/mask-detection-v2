import cv2
import onnx
import torch
import logging
import argparse
import numpy as np
import onnxruntime as ort
from utils import *
from PIL import Image
from model.model import Classifier
from onnx_tf.backend import prepare


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
buffer = None


def predict_face(width, height, confidences, boxes, prob_threshold, iou_threshold=0.5, top_k=-1):
    """
    Select boxes that contain human faces
    Args:
        width: original image width
        height: original image height
        confidences (N, 2): confidence array
        boxes (N, 4): boxes array in corner-form
        iou_threshold: intersection over union threshold.
        top_k: keep top_k results. If k <= 0, keep all the results.
    Returns:
        boxes (k, 4): an array of boxes kept
        labels (k): an array of labels for each boxes kept
        probs (k): an array of probabilities for each boxes being in corresponding labels
    """
    boxes = boxes[0]
    confidences = confidences[0]
    picked_box_probs = []
    picked_labels = []
    for class_index in range(1, confidences.shape[1]):
        probs = confidences[:, class_index]
        mask = probs > prob_threshold
        probs = probs[mask]
        if probs.shape[0] == 0:
            continue
        subset_boxes = boxes[mask, :]
        box_probs = np.concatenate([subset_boxes, probs.reshape(-1, 1)], axis=1)
        box_probs = hard_nms(box_probs,
           iou_threshold=iou_threshold,
           top_k=top_k,
           )
        picked_box_probs.append(box_probs)
        picked_labels.extend([class_index] * box_probs.shape[0])
    if not picked_box_probs:
        return np.array([]), np.array([]), np.array([])
    picked_box_probs = np.concatenate(picked_box_probs)
    picked_box_probs[:, 0] *= width
    picked_box_probs[:, 1] *= height
    picked_box_probs[:, 2] *= width
    picked_box_probs[:, 3] *= height
    return picked_box_probs[:, :4].astype(np.int32), np.array(picked_labels), picked_box_probs[:, 4]


def predict_mask(model, faces, preprocess_face, frame_idx, save_box=False):
    """
    Return list of scores given list face
    Args:
        preprocess_face: pre-process standard
        model: classifier
        faces: ndarray
    """
    global buffer
    score_nomask = []
    face_idx = 0

    for face in faces:
        face, image_path = cv2toPIL(buffer, face, frame_idx, face_idx)
        face = preprocess_face(face)
        prediction = model(face.unsqueeze(0))              # prediction shape: (2,)
        
        m = torch.nn.Softmax()
        score = m(prediction)[0]
        score_nomask.append(score)
        face_idx += 1

        # decide save box or not
        if save_box == False:
            cleanup(image_path)

    return score_nomask


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_video", type=str, default="mask-detection-v2/examples/IMG_0090.mp4",
        help="path to inference video")
    parser.add_argument("--frame_period", type=int, default=3,
        help="period process frame, ex: 5, after 5 frame, engine process with ones")
    parser.add_argument("--buffer", type=str, default='mask-detection-v2/buffer',
        help="path to folder contains images box ectracted from face detector")
    parser.add_argument("--face_model", type=str, default='mask-detection-v2/pretrain_model/ultra_light_640.onnx',
        help="path to pre-trained faceboxs detector model")
    parser.add_argument("--checkpoint", type=str, default='mask-detection-v2/pretrain_model/model_best_112.pth.tar',
        help="path to checkpoint classifier")
    parser.add_argument("--nomask_confidence", type=float, default=0.8,
        help="minimum probability to filter weak detections of mask")
    parser.add_argument("--face_confidence", type=float, default=0.5,
        help="minimum probability to filter weak detections of faceboxs")
    args = parser.parse_args()

    maybe_create_folder(args.buffer)
    global buffer
    buffer = args.buffer

    # load face model
    # onnx_model = onnx.load(args.face_model)
    # predictor = prepare(onnx_model)
    ort_session = ort.InferenceSession(args.face_model)
    input_name = ort_session.get_inputs()[0].name

    # load mask model pytorch
    checkpoint = torch.load(args.checkpoint, map_location=torch.device('cpu'))
    mask_detector = Classifier()
    mask_detector.load_state_dict(checkpoint['classifier'])
    preprocess_face = load_preprocess()

    # read video
    video_capture = cv2.VideoCapture(args.input_video)
    frame_idx = 0

    while True:
        _, frame = video_capture.read()
        if frame is not None:
            if frame_idx % args.frame_period == 0:
                frame_idx += 1
            else:
                frame_idx += 1
                continue
            frame = cv2.resize(frame, (1280, 720))
            h, w, _ = frame.shape

            # preprocess img acquired and predict face boxs
            img = preprocess_img(frame)
            confidences, boxes = ort_session.run(None, {input_name: img})
            boxes, _, _ = predict_face(w, h, confidences, boxes, args.face_confidence)
            faces, boxes = get_faces(boxes, frame)
            
            # detect mask
            if faces.shape[0] > 0:
                score = predict_mask(mask_detector, faces, preprocess_face, frame_idx)

            # draw boxs
            for i in range(len(boxes)):    
                box = boxes[i]
                x1, y1, x2, y2 = box
                if score[i] > args.nomask_confidence:
                    logger.info("predict: {}".format(score[i]))
                    label = "nomask: {:.2f}%".format(score[i])
                else:
                    continue
                cv2.putText(frame, label, (x1, y1 - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
                cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 0, 255), 2)

            cv2.imshow("video", frame)
        else:
            break

        # Hit 'q' on the keyboard to quit!
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        
    # Release handle to the webcam
    video_capture.release()
    cv2.destroyAllWindows()


if __name__=="__main__":
    logger.info('__main__')
    main()