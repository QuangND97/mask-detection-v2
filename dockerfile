FROM ubuntu:18.04
# Install some basic utilities
RUN apt-get update && apt-get install -y \
    curl \
    ca-certificates \
    sudo \
    git \
    bzip2 \
    libx11-6 \
    python3.7 \
    python3-pip\
    nano \
 && rm -rf /var/lib/apt/lists/*
WORKDIR /apps

RUN apt-get update
RUN apt-get install -y libsm6 libxext6 libxrender-dev
RUN python3 -m pip install --upgrade pip

# All users can use /home/user as their home directory
RUN git clone https://QuangND97@bitbucket.org/QuangND97/mask-detection-v2.git
RUN pip3 install -r mask-detection-v2/requirements.txt
ENTRYPOINT [ "python3","mask-detection-v2/detect.py" ]