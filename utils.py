import os
import cv2
import torch
import errno
import numpy as np
from PIL import Image
import torchvision.transforms as transforms 


def area_of(left_top, right_bottom):
    """
    Compute the areas of rectangles given two corners.
    Args:
        left_top (N, 2): left top corner.
        right_bottom (N, 2): right bottom corner.
    Returns:
        area (N): return the area.
    """
    hw = np.clip(right_bottom - left_top, 0.0, None)
    return hw[..., 0] * hw[..., 1]


def iou_of(boxes0, boxes1, eps=1e-5):
    """
    Return intersection-over-union (Jaccard index) of boxes.
    Args:
        boxes0 (N, 4): ground truth boxes.
        boxes1 (N or 1, 4): predicted boxes.
        eps: a small number to avoid 0 as denominator.
    Returns:
        iou (N): IoU values.
    """
    overlap_left_top = np.maximum(boxes0[..., :2], boxes1[..., :2])
    overlap_right_bottom = np.minimum(boxes0[..., 2:], boxes1[..., 2:])

    overlap_area = area_of(overlap_left_top, overlap_right_bottom)
    area0 = area_of(boxes0[..., :2], boxes0[..., 2:])
    area1 = area_of(boxes1[..., :2], boxes1[..., 2:])
    return overlap_area / (area0 + area1 - overlap_area + eps)


def hard_nms(box_scores, iou_threshold, top_k=-1, candidate_size=200):
    """
    Perform hard non-maximum-supression to filter out boxes with iou greater
    than threshold
    Args:
        box_scores (N, 5): boxes in corner-form and probabilities.
        iou_threshold: intersection over union threshold.
        top_k: keep top_k results. If k <= 0, keep all the results.
        candidate_size: only consider the candidates with the highest scores.
    Returns:
        picked: a list of indexes of the kept boxes
    """
    scores = box_scores[:, -1]
    boxes = box_scores[:, :-1]
    picked = []
    indexes = np.argsort(scores)
    indexes = indexes[-candidate_size:]
    while len(indexes) > 0:
        current = indexes[-1]
        picked.append(current)
        if 0 < top_k == len(picked) or len(indexes) == 1:
            break
        current_box = boxes[current, :]
        indexes = indexes[:-1]
        rest_boxes = boxes[indexes, :]
        iou = iou_of(
            rest_boxes,
            np.expand_dims(current_box, axis=0),
        )
        indexes = indexes[iou <= iou_threshold]

    return box_scores[picked, :]


def preprocess_img(frame):
    """
    Preprocessing Frame for predict faces boxs
    Input: frame
    Ouput: frame after preprocessing 
    """
    img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB) # convert bgr to rgb
    img = cv2.resize(img, (640, 480)) # resize
    img_mean = np.array([127, 127, 127])
    img = (img - img_mean) / 128
    img = np.transpose(img, [2, 0, 1])
    img = np.expand_dims(img, axis=0)
    img = img.astype(np.float32)

    return img


def get_faces(boxes, frame):
    """
    Crop frame to faces using boxes
    Args:
            boxes: bounding box
            frame: frame
    Return: list of faces in frame
            clean_boxes: list of ndarray
    """
    faces =[]
    clean_boxes = []
    for i in range(boxes.shape[0]):
        startX, startY, endX, endY = boxes[i,:]
        face = frame[startY:endY, startX:endX]

        try:
            face = cv2.resize(face, (112, 112))
            faces.append(face)
            clean_boxes.append(boxes[i, :])
        except:
            continue
        
    return np.array(faces, dtype="float32").reshape(-1, 112, 112, 3), clean_boxes


def load_preprocess():
    return transforms.Compose([
    transforms.Resize(112),
    transforms.CenterCrop(112),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])


def cv2toPIL(buffer, face, frame_idx, face_idx):
    """
    Convert to PIL Image
    """
    image_path = os.path.join(buffer, str(frame_idx) + '_' +  str(face_idx) + '.jpg') 
    cv2.imwrite(image_path, face)
    return Image.open(image_path), image_path


def cleanup(image_path):
    os.remove(image_path)


def maybe_create_folder(folder):
    try:
        os.makedirs(folder)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
