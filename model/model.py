import torch.nn.functional as F
import torch.nn as nn
import torch


class Classifier(nn.Module):
    def __init__(self):
        super(Classifier, self).__init__()
        self.model = torch.hub.load('pytorch/vision:v0.6.0', 'mobilenet_v2', pretrained=True)
        self.model.eval()
        self.model.classifier = nn.Sequential(*list(self.model.classifier.children())[:-3])
        self.set_parameter_requires_grad(self.model, True)

        self.layers = [512, 128, 128, 64, 8]

        self.fc = nn.ModuleList()
        self.drop = nn.ModuleList()

        # connect output of image extractor vs first hidden layer
        fc = nn.Linear(1280, self.layers[0], bias=True)
        nn.init.kaiming_uniform_(fc.weight, nonlinearity='relu')
        self.fc.append(fc)
        self.drop.append(nn.Dropout(p=0.5))

        for size_current, size_next in zip(self.layers[:-1], self.layers[1:]):
            fc = nn.Linear(size_current, size_next, bias=True)
            nn.init.kaiming_uniform_(fc.weight, nonlinearity='relu')

            self.fc.append(fc)
            self.drop.append(nn.Dropout(p=0.5))

        # connect output of hidden layer vs 2 node
        self.fc_last = nn.Linear(self.layers[-1], 2, bias=True)
        nn.init.kaiming_uniform_(self.fc_last.weight, nonlinearity='relu')


    def set_parameter_requires_grad(self, model, feature_extracting):
        param_idx = 0
        for param in self.model.parameters():
            if param_idx > 100:
                param.requires_grad = True
            else:
                param.requires_grad = False
            param_idx += 1

    def forward(self, x):
        x = self.model(x).squeeze(0)
        for drop, fc in zip(self.drop, self.fc):
            x = drop(F.relu(fc(x)))
        x = self.fc_last(x)
        return x