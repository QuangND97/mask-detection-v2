# **Installation**
1. Install **python 3.7 and pip 20.2.2**
2. Install requirements for python using pip
```bash
pip3 install -r mask-detection-v2/requirements.txt
```

# **Run file**
Engine input parameters
1. **Nomask_confidence**: Threshold of mask scores which are predicted by facebox model, default = 0.8
2. **Face_confidence**: Threshold of faceboxes scores which are predictd by mask model, default = 0.5
3. **Frame_period**: Number of frames passed during prediction. Example: frame_step = 10 means that model will predict 1 frames in 10 consecutive frames. Default = 3
4. **Input_video**: Link to video file or link video stream (example: RTSP Link)

Example of run in command:
```bash
python3 mask-detection-v2/detect.py --nomask_confidence 0.8 --face_confidence 0.5 --frame_period 3 --input_video mask-detection-v2/examples/IMG_0090.mp4
```

If you wanted to use default setup:
```bash
python3 mask-detection-v2/detect.py
```